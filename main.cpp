#include <iostream>
#include "vect.hpp"
#include <random>
#include "random.hpp"
using namespace std;
using namespace math;

const int stop = 100000000;

#include <array>
#include <boost/progress.hpp>
using namespace std;
using namespace boost;
using namespace Eigen;

template<size_t N, class T = double>
class shared_vect : public Map<Matrix<T, N, 1> > {
public:
  shared_vect ( T* data ) : Map<Matrix<T, N, 1> >( data, N, 1 ) {
  }

  template<typename OtherDerived>
  shared_vect ( const Map<OtherDerived>& other ) : Map<Matrix<T, N, 1> >( other ) {
  }

  template<typename OtherDerived>
  shared_vect ( const MatrixBase<OtherDerived>& other ) : Map<Matrix<T,N,1> >( other ) {
  }
};

#define N 200
#define M 200

int main ( ) {
  array<double,N*M + N> memory;
  Random gen;
  for ( int i =0 ; i < N*M+N; ++i ) memory[i] = gen.realnegative() / 100;

  Map<Matrix<double, N, M, RowMajor> > mappe( memory.data() );
  shared_vect<N> vettore( memory.data() + N * M );

  progress_timer t;
  for ( int i = 0; i < 10000; ++i ) {
    vettore = mappe * vettore;
  }

  cout << vettore[0] << endl;

  /*matrix<3,3,double> a = { 9,8,7, 6,5,4, 3,2,1 };
  vect<3,double> b = { 1,2,3 };
  vect<3,double> zero;
  cout << a * b << endl;
  cout << zero << endl;
  cout << a.eigenvalues() << endl;
  cout << a.eigenvectors() << endl;*/


//  Random gen;
//  double val = 0.0;
//  for ( int i = 0; i < stop; ++i ) {
//    val += gen.real();
//  }

//  double val = 0.0;
//  default_random_engine eng( 0 );
//  mt19937 eng( 0 );
//  uniform_real_distribution<double> uniform_real(0.0,1.0);
//  for ( int i = 0; i < stop; ++i ) {
//    val += uniform_real(eng);
//  }

  //cout << val / stop << endl;

  return 0;
}
